var path = require ("path");
var express= require("express");

//create instance

var app= express();

//create route
app.use(express.static(path.join(__dirname,"public")));
app.use("/libs", express.static(path.join(__dirname, "bower_components")));

//set port
//app.set("port", 3000)
app.set("port", parseInt(process.argv[2]) || 3000);





app.listen(app.get("port"), function(){
console.info("Application is listening on port " + app.get("port"));

});

